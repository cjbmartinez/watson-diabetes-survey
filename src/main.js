import Vue from 'vue'
import App from './App.vue'
import AutoResponsive from 'autoresponsive-vue';
import VueResource from 'vue-resource';
import VueMq from 'vue-mq'
import VueClazyLoad from 'vue-clazy-load';
import VueProgressiveImage from 'vue-progressive-image'

Vue.use(VueResource);
Vue.http.options.root = 'https://watson-survey.firebaseio.com/';
Vue.use(AutoResponsive);
new Vue({
  el: '#app',
  render: h => h(App)
})
